# APO CacheBench

Benchmarking utilities for HW2 from B0B35APO.

## Running on Linux

1. Download Docker for your operating system: https://docs.docker.com/get-docker/
2. Add yourself to the `docker` system group so that you can run docker container without root privileges.
3. Download the script [`acbench`](https://gitlab.fel.cvut.cz/vanekj19/apo-cachebench/-/raw/master/acbench?inline=false) to the directory with the sources of your APO homework.
4. Make the script executable:
   ```
   chmod +x acbench
   ```
4. Run the benchmark like this:
   ```
   ./acbench
   ```

## Running on Windows

1. Download Docker for Windows
2. Enable running Docker as a non-privileged user: https://stackoverflow.com/questions/40459280/docker-cannot-start-on-windows
3. You can run the test using the following command from Powershell (replace `/c/path/to/source/directory` accordingly, e.g. `/c/work/apo`):
   ```
   docker run --rm -it -v "//c/path/to/source/directory:/eval/src" linuxtardis/apocachebench
   ```
   If you're using Git Bash, you will need to run it like this:
   ```
   winpty docker run --rm -it -v "//c/path/to/source/directory:/eval/src" linuxtardis/apocachebench
   ```
