#!/bin/bash

RED="$(echo -e "\e[31m")"
GREEN="$(echo -e "\e[32m")"
YELLOW="$(echo -e "\e[33m")"
RST="$(echo -e "\e[0;10m")"


# compile program
if [ ! -f /eval/src/main.c ] && [ ! -f /eval/src/main.cpp ]; then
    echo "${RED}ERROR:${RST} main.c not found" >&2
    echo "- please rerun this container with your source directory mounted at /eval/src."
    exit 1

elif [ -f /eval/src/main.cpp ]; then
    echo "-- found main.cpp, compiling" >&2
    cd /eval/src
    g++ -mssse3 -g -O1 -Wall -Werror -std=gnu++11 -o /eval/prg main.cpp -lm
    STATUS="$?"

elif [ -f /eval/src/main.c ]; then
    echo "-- found main.c, compiling" >&2
    cd /eval/src
    gcc -mssse3 -g -O1 -Wall -Werror -std=c99 -o /eval/prg main.c -lm
    STATUS="$?"
fi

if [ "$STATUS" -ne 0 ]; then
    echo "${RED}ERROR:${RST} program compilation failed." >&2
    exit 2
else
    echo "${YELLOW}OK:${RST} compilation was successful" >&2
    echo >&2
fi

# evaluate program
cd /eval

echo "-- testing program for memory leaks on vit_normal.ppm" >&2
valgrind --error-exitcode=1 --tool=memcheck --leak-check=full --show-leak-kinds=all --track-origins=yes /eval/prg /eval/vit_normal.ppm
STATUS="$?"

if [ "$STATUS" -ne 0 ]; then
    echo "${RED}ERROR:${RST} valgrind and/or program failed." >&2
    exit 3
else
    echo "${YELLOW}OK:${RST} no leaks detected" >&2
    echo >&2
fi

echo "-- evaluating performance on vit_normal.ppm" >&2
valgrind --tool=cachegrind --I1=32768,8,64 --D1=32768,8,64 --LL=1048576,16,64 --cachegrind-out-file=/eval/src/cachegrind_result /eval/prg /eval/vit_normal.ppm
STATUS="$?"

if [ "$STATUS" -ne 0 ]; then
    echo "${RED}ERROR:${RST} cachegrind and/or program failed." >&2
    exit 3
else
    echo "${YELLOW}OK:${RST} program finished successfully, profiling info exported to cachegrind_result" >&2
    echo >&2
fi

echo "-- calculating extra parameters:"
python3 /eval/eval.py /eval/src/cachegrind_result
echo >&2

echo "-- checking histogram"
if ! cmp /eval/hist_normal.txt /eval/output.txt; then
    echo "${RED}ERROR:${RST} Histograms do not match!" >&2
    exit 4
else
    echo "${GREEN}SUCCESS:${RST} Histograms match!" >&2
fi

exit 0
