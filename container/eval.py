#!/usr/bin/env python3

import os
import sys

def main():
    if len(sys.argv) != 2:
        print("error: expected cachegrind output file as the only input argument", file=sys.stderr)
        sys.exit(1)

    with open(sys.argv[1], "r") as fp:
        lines = [line for line in fp]
        summary = lines[-1]
        fields = summary.split()

    Irefs    = int(fields[1])
    I1misses = int(fields[2])
    I2misses = int(fields[3])

    Drefs    = int(fields[4]) + int(fields[7])
    D1misses = int(fields[5]) + int(fields[8])
    D2misses = int(fields[6]) + int(fields[9])

    HT_L1i = 0.6e-9
    HT_L1d = 0.6e-9
    HT_L2  = 6.0e-9
    HT_RAM = 143e-9

    MR_L1i = I1misses / Irefs
    MR_L2i = I2misses / I1misses
    MR_L1d = D1misses / Drefs
    MR_L2d = D2misses / D1misses

    AMAT_i = HT_L1i + MR_L1i*(HT_L2 + MR_L2i*HT_RAM)
    AMAT_d = HT_L1d + MR_L1d*(HT_L2 + MR_L2d*HT_RAM)

    ICost = AMAT_i*Irefs
    DCost = AMAT_d*Drefs
    Cost = ICost + DCost

    Cost_Min = 1.0
    Cost_Max = 5.0
    
    Body_efektivita = 13*(Cost_Max-Cost)/(Cost_Max - Cost_Min)
    Body_celkem = Body_efektivita + 1

    if Body_celkem < 1.0:
        Body_celkem = 1.0
    elif Body_celkem > 14.0:
        Body_celkem = 14.0

    bonus = Cost < 0.31
    if bonus:
        Body_celkem += 5

    print(f"L1i adjusted miss rate: {MR_L1i*1e2:4.1f} %")
    print(f"L2i adjusted miss rate: {MR_L2i*1e2:4.1f} %")
    print(f"L1d adjusted miss rate: {MR_L1d*1e2:4.1f} %")
    print(f"L2d adjusted miss rate: {MR_L2d*1e2:4.1f} %")
    print()

    print(f"AMAT data: {AMAT_d*1e9:.6f} ns")
    print(f"AMAT insn: {AMAT_i*1e9:.6f} ns")
    print()

    print(f"Data  fetch cost: {DCost:,.3f} s")
    print(f"Instr fetch cost: {ICost:,.3f} s")
    print(f"Total fetch cost: {Cost:,.3f} s")
    print()

    if not bonus:
        print(f"Points: {Body_celkem:.1f} pts")
    else:
        print(f"Points: {Body_celkem:.1f} pts (well done, +5 bonus points awarded)")

    sys.exit(0)

if __name__ == "__main__":
    main()

